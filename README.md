# mybb-internal_links

internal_links plugin for mybb

Add mycode :
- `[forum=x][/forum]` : link to forum number x, with automatic title and text from forum title. Text can be updated inside mycode : `[forum=x]text of the link[/forum]`.
- `[thread=x][/thread]` : link to thread number x, with automatic title and text from thread title. Text can be updated inside mycode: `[thread=x]text of the link[/thread]`.
- `[post=x][/post]` : link to post number x, with automatic title and text from post title. Text can be updated inside mycode: `[post=x]text of the link[/post]`.
