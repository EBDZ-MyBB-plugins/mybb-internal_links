<?php
/*
 * Internal Links
 * @author: Mattia
 * @author: Betty
 * @version 1.1.1
 */

// Disallow direct access to this file for security reasons
if (!defined('IN_MYBB')) {
	die('Direct initialization of this file is not allowed.');
}

function internal_links_info() {
	return array(
		'name' => 'Internal Links',
		'description' => 'MyCode for internal links',
		'website' => '',
		'author' => 'Mattia',
		'authorsite' => 'https://www.matriz.it/',
		'version' => '1.1.0',
		'codename' => str_replace('.php', '', basename(__FILE__)),
		'compatibility' => '*'
	);
}
 
$plugins->add_hook('parse_message_end', 'internal_links_parse_message');

/**
 * Modifica il testo del messaggio sostituendo i MyCode dei link interni
 * @param string $msg testo del messaggio
 * @return string
 */
function internal_links_parse_message($msg) {
	// Forum
	$matches = array();
	if (preg_match_all('/\[forum\=([0-9]+)\](.*?)\[\/forum\]/i', $msg, $matches, PREG_SET_ORDER)) {
		$counter = count($matches);
		for ($i = 0; $i < $counter; $i++) {
			$forum = get_forum($matches[$i][1]);
			if ($forum) {
				$text = $matches[$i][2];
				if (empty($text)) {
					$text = htmlspecialchars_uni($forum['name']);
				}
				$msg = str_replace($matches[$i][0], '<a href="'.get_forum_link($forum['fid']).'" title="'.htmlspecialchars_uni($forum['name']).'">'.$text.'</a>', $msg);
			}
			unset($forum);
		}
		unset($i, $counter);
	}
	unset($matches);
	
	// Thread
	$matches = array();
	if (preg_match_all('/\[thread\=([0-9]+)\](.*?)\[\/thread\]/i', $msg, $matches, PREG_SET_ORDER)) {
		$counter = count($matches);
		for ($i = 0; $i < $counter; $i++) {
			$thread = get_thread($matches[$i][1]);
			if ($thread) {
				$text = $matches[$i][2];
				if (empty($text)) {
					$text = htmlspecialchars_uni($thread['subject']);
				}
				$msg = str_replace($matches[$i][0], '<a href="'.get_thread_link($thread['tid']).'" title="'.htmlspecialchars_uni($thread['subject']).'">'.$text.'</a>', $msg);
			}
			unset($thread);
		}
		unset($i, $counter);
	}
	unset($matches);
	
	// Post
	$matches = array();
	if (preg_match_all('/\[post\=([0-9]+)\](.*?)\[\/post\]/i', $msg, $matches, PREG_SET_ORDER)) {
		$counter = count($matches);
		for ($i = 0; $i < $counter; $i++) {
			$post = get_post($matches[$i][1]);
			if ($post) {
				$text = $matches[$i][2];
				if (empty($text)) {
					$text = htmlspecialchars_uni($post['subject']);
				}
				$msg = str_replace($matches[$i][0], '<a href="'.get_post_link($post['pid'], $post['tid']).'#pid'.$post['pid'].'" title="'.htmlspecialchars_uni($post['subject']).'">'.$text.'</a>', $msg);
			}
			unset($post);
		}
		unset($i, $counter);
	}
	unset($matches);
	
	return $msg;
}
